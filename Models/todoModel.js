class TodoModel {
    status;
    name;

    setToLocalstorage() {
        let todo = TodoModel.getFromLocalstorage(this.name)
        if (todo == null) {
            localStorage.setItem(this.name, JSON.stringify(this));
            return true;
        }
        return false;
    }

    static getFromLocalstorage(name) {
        return localStorage.getItem(name)
    }

    static getAllFromLocalstorage() {
        return Object.keys(localStorage).map(key => JSON.parse(localStorage.getItem(key)));
    }

    static deleteFromLocalstorage(name) {
        localStorage.removeItem(name);
    }

    editFromLocalstorage(oldName) {
        let todo = TodoModel.getFromLocalstorage(oldName)
        if (todo == null) {
            return false;
        }
        TodoModel.deleteFromLocalstorage(oldName)
        localStorage.setItem(this.name, JSON.stringify(this));
        return true;
    }

    toString() {
        return `Todo: ${this.name}, Status: ${this.status}`;
    }
}