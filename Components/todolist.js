angular.module('todolistApp')
    .component('showTodolist', {
        template: `
        <h1>Todo List</h1>
        <button ng-click="$ctrl.isAdd = true"> Add</button>
        <button ng-click="$ctrl.deleteCheckedTodo()">Delete Checked</button>
        <button ng-click="$ctrl.deleteAllTodos()">Delete All</button>
        <button ng-click="$ctrl.emptyCache()">Empty cache</button>
        <table>
            <tr>
                <th>
                <input type="checkbox" ng-model="$ctrl.checkAll" ng-click="$ctrl.selectAll()"/>
                </th>
                <th>Name</th>
                <th>status</th>
                <th>edit</th>
                <th>delete</th>
            </tr>
            <tr ng-repeat="todo in $ctrl.todos">
                <td>
                    <input type="checkbox" ng-model="todo.checked" ng-click="$ctrl.allChecked()"/>
                </td>
                <td>
                    {{ todo.name }}
                </td>
                <td>
                    {{ todo.status }}
                </td>
                <td>
                    <button ng-click="$ctrl.editTodo(todo)">Edit</button>
                </td>
                <td>
                    <button ng-click="$ctrl.deleteTodo(todo)">Delete</button>
                </td>
            </tr> 
        </table>
        <div ng-if="$ctrl.isAdd">
            <h2>Add todo</h2>
            <form ng-submit="$ctrl.submitFormAdd($ctrl.todoAdd)">
                <label for="todoNameAdd">Todo name:</label><br>
                <input type="text" id="todoNameAdd" ng-model="$ctrl.todoAdd.name">
                <br>
                <label for="todoStatusAdd">Status:</label><br>
                <input type="text" id="todoStatusAdd" ng-model="$ctrl.todoAdd.status">
                <br>
                <button ng-click="$ctrl.isAdd = false">Cancel</button>
                <input type="submit" value="Submit">
            </form>
        </div>
        <div ng-if="$ctrl.isEdit">
            <h2>Edit todo</h2>
            <form ng-submit="$ctrl.submitFormEdit($ctrl.todo)">
                <label for="todoName">Todo name:</label><br>
                <input type="text" id="todoName" ng-model="$ctrl.todo.name">
                <br>
                <label for="todoStatus">Status:</label><br>
                <input type="text" id="todoStatus" ng-model="$ctrl.todo.status">
                <br>
                <button ng-click="$ctrl.isEdit = false">Cancel</button>
                <input type="submit" value="Submit">
            </form>
        </div>
        `,
        controller: function () {
            this.todoOldName = "";

            this.$onInit = function () {
                this.todos = TodoModel.getAllFromLocalstorage();
            }

            this.submitFormEdit = function (todo) {
                let todoModel = this.hydrateTodo(todo);
                todoModel.editFromLocalstorage(this.todoOldName);
                this.todos = TodoModel.getAllFromLocalstorage();
                this.todo = new TodoModel();
                this.isEdit = false;
            }

            this.submitFormAdd = function (todo) {
                let todoModel = this.hydrateTodo(todo);
                todoModel.setToLocalstorage();
                this.todos = TodoModel.getAllFromLocalstorage();
                this.todoAdd = new TodoModel();
                this.isAdd = false;
            }

            this.hydrateTodo = function (todo) {
                let todoModel = new TodoModel();
                todoModel.name = todo.name;
                todoModel.status = todo.status;
                return todoModel;
            }

            this.editTodo = function (todo) {
                this.isEdit = true;
                this.todo = angular.copy(todo);
                this.todoOldName = todo.name;
            }

            this.setTodo = function (todo) {
                todo.setToLocalstorage();
                this.todos.push(todo);
            }

            this.deleteTodo = function (todo) {
                TodoModel.deleteFromLocalstorage(todo.name);
                this.todos = this.todos.filter(td => td !== todo);
            }

            this.deleteAllTodos = function () {
                this.todos.forEach(todo => this.deleteTodo(todo));
            }

            this.emptyCache = function () {
                Object.keys(localStorage).forEach(key => localStorage.removeItem(key));
                window.location.reload();
            }

            this.allChecked = function () {
                if (!this.todos.find(todo => !todo.checked)) {
                    this.checkAll = true;
                }
                if (this.todos.find(todo => !todo.checked)) {
                    this.checkAll = false;
                }
            }

            this.selectAll = function () {
                if (!this.checkAll) {
                    this.todos.forEach(todo => todo.checked = false);
                }
                else {
                    this.todos.forEach(todo => todo.checked = true);
                }
            }

            this.deleteCheckedTodo = function () {
                this.todos = this.todos.filter(todo => {
                    if(todo.checked) {
                        this.deleteTodo(todo);
                    }
                    else {
                        return todo;
                    }
                })
            }
        }
    })